<?php

function menu_alias_admin($form, $form_state) {
  $form['menu_alias_from'] = array(
    '#type' => 'textfield',
    '#title' => t('Change from'),
    '#required' => TRUE,
    '#default_value' => variable_get('menu_alias_from', ''),
  );

  $form['menu_alias_to'] = array(
    '#type' => 'textfield',
    '#title' => t('Change to'),
    '#required' => TRUE,
    '#default_value' => variable_get('menu_alias_to', ''),
  );

  return system_settings_form($form);
}
